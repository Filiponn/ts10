import './App.css';
import Navbar from "./components/Navbar";

// npm install react-router-dom@6
// npm i mathjs
// npm install @mui/material @emotion/react @emotion/styled
// npm i @mui/system
// npm i @mui/styled-engine
// npm i @mui/material

function App() {


    return (
        <div className="App">
            <Navbar></Navbar>
        </div>
    );
}

export default App;