import * as React from 'react';

import Table from "./Table";
import {useLocation} from "react-router-dom";


const LadiesRes = () => {
    const location = useLocation();
    return (
        <div>
            <Table data={location.state.ladies}></Table>
        </div>
    );
};
export default LadiesRes;