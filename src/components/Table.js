import {
    Table,
    TableBody,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    TableCell,
    styled,
    tableCellClasses,
    Container,
    Button,
    ButtonGroup
} from '@mui/material';
import {useEffect, useState} from 'react';

const TablePlayers = (props) => {
    const [rows, setRows] = useState(props.data);

    useEffect(() => {
        setRows(props.data);
    }, [props.data])

    const sortAsc = () => {
        const newRows = [...rows];
        setRows(newRows.sort((a, b) => a.totalWins - b.totalWins))
    };

    const sortDesc = () => {
        const newRows = [...rows];
        setRows(newRows.sort((a, b) => b.totalWins - a.totalWins))
    };

    const StyledTableCell = styled(TableCell)(({theme}) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({theme}) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.focus,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));
    return (
        <Container>
            <ButtonGroup style={{paddingRight:"50px", paddingTop:"20px"}}>
                <Button onClick={sortAsc} variant="contained" style={{backgroundColor:"dodgerblue"}}>
                    Sort by number of titles ascending
                </Button>
            </ButtonGroup>
            <ButtonGroup>
                <Button onClick={sortDesc} variant="contained" style={{backgroundColor:"darkred"}}>
                    Sort by number of titles descending
                </Button>
            </ButtonGroup>
            <TableContainer component={Paper} align="center" style={{paddingTop:"10px"}}>
                <Table sx={{maxWidth: 1200}} aria-label="customized table">
                    <TableHead>
                        <TableRow>

                            <StyledTableCell align="center">Years</StyledTableCell>
                            <StyledTableCell align="center">Years</StyledTableCell>
                            <StyledTableCell align="center">Titles</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <StyledTableRow key={row.name}>
                                <StyledTableCell align="center">{row.name}</StyledTableCell>
                                <StyledTableCell align="center">{row.years}</StyledTableCell>
                                <StyledTableCell align="center">{row.totalWins}</StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    );
}
export default TablePlayers;