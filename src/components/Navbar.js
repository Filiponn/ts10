import {AppBar, Container, MenuItem, Toolbar, Typography} from "@mui/material";
import {Link, NavLink, Route, Routes} from "react-router-dom";
import MainPage from "./MainPage";
import GentelmensRes from "./GentelmensRes";
import LadiesRes from "./LadiesRes";
import ManIcon from '@mui/icons-material/Man';
import WomanIcon from '@mui/icons-material/Woman';

const Navbar = () => {
    // https://medium.com/frontendweb/how-to-pass-state-or-data-in-react-router-v6-c366db9ee2f4

    return (
        <div>
            <AppBar position="static" style={{ background: 'green' }}>

                <Container maxWidth="xl">
                    <Toolbar>
                        <Link to="/">
                            <MenuItem>
                                <img src="https://seeklogo.com/images/W/Wimbledon-logo-0BC22D8FAF-seeklogo.com.png" alt="logo" width="100px" height="auto"/>
                                <Typography variant="h6" style={{ color: 'white', paddingLeft:"10px"}}>
                                    Home
                                </Typography>
                            </MenuItem>
                        </Link>

                    </Toolbar>
                </Container>
            </AppBar>


            <Routes>
                <Route path="/" element={<MainPage/>}>
                </Route>
                <Route path="/gentelmens" element={<GentelmensRes/>}>
                </Route>
                <Route path="/ladies" element={<LadiesRes/>}>
                </Route>

            </Routes>

        </div>

    );


}

export default Navbar;