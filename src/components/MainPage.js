import {Button, ButtonGroup, Container} from "@mui/material";
import {Link} from "react-router-dom";
import {useEffect, useState} from "react";
import wimbledonChampions from "wimbledon-champions";

const MainPage = () => {

    const [gentelmens, setGentelmens] = useState([]);
    const [ladies, setLadies] = useState([]);
    let gent =[];
    let lad = [];
    useEffect(() => {

        wimbledonChampions().forEach(item=>{

            if(item.titles[0].title.startsWith("Ladies")){
                let years = item.titles.map(title => (title.year)).join(', ');
                let name = item.name;
                let totalWins = 0;

                totalWins = years.length;
                lad.push({name, totalWins, years})
            } else{
                let years = item.titles.map(title => (title.year)).join(', ');
                let name = item.name;
                let totalWins = 0;

                totalWins = years.length;

                gent.push({name, totalWins, years})
            }
            setLadies(lad);
            setGentelmens(gent)
        });

        }, [])
    console.log(ladies)
    console.log(gentelmens)

    return (
        <div>
            <div>
                <Container style={{paddingTop:"50px"}}>
                <ButtonGroup style={{paddingRight:"50px"}}>
                    <Link to="/ladies" state={{ladies}}>
                    <Button size="large" variant="contained" style={{backgroundColor:"cornflowerblue"}}>
                        GENTELMENS
                    </Button>
                    </Link>
                </ButtonGroup>
                    <ButtonGroup>
                        <Link to="/gentelmens" state={{gentelmens}}>
                        <Button size="large" variant="contained" style={{backgroundColor:"violet"}}>
                           LADIES
                        </Button>
                        </Link>
                    </ButtonGroup>
                </Container>
            </div>
        </div>

    );


}

export default MainPage;