import * as React from 'react';
import {useEffect, useState} from "react";
import wimbledonChampions from "wimbledon-champions";
import Table from "./Table";
import {useLocation} from "react-router-dom";

const GentelmensRes= () => {
    const location = useLocation();
    return (
        <div>
            <Table data={location.state.gentelmens}></Table>
        </div>
    );
};
export default GentelmensRes;